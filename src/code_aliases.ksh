# aliases for code_* functions
alias code_bdiff=code_bdiff_git.ksh
alias code_checkout=code_checkout_git.ksh
alias checkout=code_checkout_git.ksh
alias code_hist=code_hist_git.ksh
alias code_stash=code_stash_git.ksh
alias code_stat=code_stat_git.ksh
alias code_synch=code_synch_git.ksh
