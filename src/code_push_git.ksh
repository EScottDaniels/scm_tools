#!/usr/bin/env ksh
# Mnemonic:		code_push_git
# Abstract:		This script will push the current set of changes which haven't
#				been sent to the remote.
#
#
# Date:		20 October 2020 (snarfed from code_stash with date of: 04 March 2012)
# Author:	E. Scott Daniels
#
# ---------------------------------------------------------------------------------

function cleanup {
	rm -f /tmp/PID$$.*
	exit ${1:-0}
}


#  Given a branch name, ensure that it exists as a remote branch
function confirm_gb {
	git branch -l -a | grep remote | grep "/${1:-kad43086}"'$'
	git branch -l -a | grep remote | grep -q "/${1:-kad43086}"'$'
	if (( $? == 0 ))
	then
		return 0
	fi

	return 1
}

# Set p to be the path to the directory with .git.
# Set rp to be what is below .git to the current directory.
# Exit if no .git is found.
function set_p {
	typeset have_setup=0

	p=$PWD				# yes, p and rp are global
	rp=""				# relative path under the .git directory to cwd

	if [[ -e .git ]]			# short circuit ensuring rp is set
	then
		rp="${p##*/}"
		return
	fi

	while true
	do
		if [[ -z $p ]]
		then
			echo "is this a git environment???  cannot find .git in this (or a parent) directory  [FAIL]"
			exit 1
		fi

		rp="/${p##*/}$rp"
		p=${p%/*}

		if (( have_setup == 0 ))  &&  [[ -f $p/.code_setup ]]	# pull in setup if we find it along the way
		then
			. $csd/.code_setup
			have_setup=1
		fi

		if [[ -e $p/.git ]]			# short circuit ensuring rp is set
		then
			return
		fi
	done
}

#  dig the current branch out of git; writes to stdout
function get_branch {
	git branch -l | awk '/^[*] / { print $2; exit( 0 ) }'
}

function usage
{
	echo "usage: $argv0 [-b branch] [-n] [-r remote-name] source-file [file...]"
}

# ------------------------------------------------------------------

set_p			# look for .git in the current dir or up the tree; bail if not found. leaves p set to the proper parent

argv0="${0##*/}"
date=""
tag_name=""
forreal=""
verbose=0
cmd="commit -s"										# force to add signed off by
msg="stashed on $(date) by $USER "
smsg_ok=${CODE_STASH_SMSGOK:-1}		# -M sets this to 0 which forces git to invoke the editor for a message (mostly for openstack things)
force_msg=${CODE_STASH_FMSG:-1}
new=0
umsg=""
remote="origin"
branch=$( get_branch )
git_opts=
trace_on="set -x"			# disabled if -n is given

push=0					# by default it is a local stash and not remote
gerrit_push=0
remote_branch=""		# for -P we need a gerrit branch and we don't default to anything!
require_gb=0			# if -P given and no -B, we'll prompt if this is set

gerrit_push=0			# by default this isn't to gerrit
know_type=0				# and by default we don't know the type, so plain -p doesn't work
if [[ -e $p/.push_type ]]
then
	head -1 $p/.push_type | read gerrit_push
	know_type=1
fi


# must support all of the options that code_stash (cvs) supports; we might ignore them, but we cannot bail on them either
while [[ $1 == -* ]]
do
	case $1 in
		-b)	remote_branch="$2"; shift;;				# if not given, and gerrit push, we prompt
		-b)	remote_branch=$2; branch=$2; shift;;

		-n)	forreal="echo would execute:"
				trace_on=""
				;;

		--push)							# flag as "nomal" (not gerrit)
			gerrit_push=0;
			echo 0 > $p/.push_type
			know_type=1
			;;

		-P)	gerrit_push=1;
			echo 1 > $p/.push_type
			know_type=1
			;;

		-r)		remote=$2; shift;;

		-v)		verbose=1;;
		-\?)	usage; cleanup 1
				;;

		-*)	error_msg "unrecognised option: $1"
			usage

			cleanup 1
			;;
	esac

	shift
done

if (( ! know_type ))
then
	echo "### ERR ###	for first push -P or --push must be used"
	echo "				use -P when pushing to gerrit"
	echo "				use --push to push to a plain git remote"
	exit 1
fi

if [[ -z $branch ]]
then
	echo "[FAIL] can't determine branch with 'git branch -l'"
	exit 1
fi


if (( gerrit_push )) && [[ -z $remote_branch ]]		# prompt for branch when pushing to gerrit
then
	good_branch=0
	while (( ! good_branch ))
	do
		ans=""
		while [[ -z $ans ]]
		do
			printf -- "push to gerrit requires a branch name (target), enter branch [? for list]: "
			read ans
		done

		if [[ $ans == "?" ]]
		then
			git branch -l -a
		else
			if confirm_gb $ans
			then
				good_branch=1
				remote_branch="$ans"
			else
				echo "branch '$ans' doesn't seem to be valid"
			fi
		fi
	done
fi

if (( gerrit_push ))
then
	log_msg "pusing to gerrit for review: $branch"
	$trace_on
	$forreal git push $remote HEAD:refs/for/${branch}
	set -x
else
	log_msg "pushing current $branch to $remote"
	$trace_on
	$forreal git push $remote $branch
fi


cleanup $?

