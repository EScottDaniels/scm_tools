#!/usr/bin/env ksh

# Mnemonic:	code_synch_git
# Abstract: Synch the current branch with upstream.
#			Done by fetching a current copy of the upstream onto the local
#			version of the repo, and then merging/rebasing in the changes.
#			Current changes must be committed.  Use -o to fetch from the remote
#			named origin rather than upstream.
# Date:		2015 sometime
# Author:	E. Scott Daniels
#-----------------------------------------------------------------------------------

merge=0
fetch=1
what=upstream

while [[ $1 == -* ]]
do
	case $1 in 
		-m) merge=1;;				# merge changes rather than rebase
		-N) fetch=0;;				# don't fetch (why?)
		-o)	what=origin;;			# easy change to remote name == origin
		-r) merge=0;;
		-R) what=$2; shift;;		# allow custom remote name

		*)	echo "unrecognised option: $1" 
			echo "usage: $0 [-o | -R remote-name] [-r|-m] [-N] [branch-name]"
			echo "-r == rebase (default); -m == merge"
			echo "-o == fetch from origin (upstream is default)"
			echo "-N == no fetch"
			exit 1
			;;
	esac

	shift
done

if [[ -z $1 ]]
then
	printf "synch with $what master? [Yn]"
	read ans
	if [[ $ans == "n"* ]]
	then
		echo "use $0 <branch-name> to synch another branch"
		echo "use -o to sinch with origin instead of upstream"
		exit
	fi
fi

if (( fetch ))
then
	echo "fetching $what"
	git fetch $what
else
	echo "fetch skipped (-n)"
fi

echo "checking out branch: ${1:-master}"
if ! git checkout ${1:-master}
then
	echo "cannot checkout ${1:-master} branch"
	exit 1
fi

if (( merge ))
then
	echo "merging $what"
	git merge $what/${1:-master}
else
	echo "rebase $what"
	git rebase $what/${1:-master}
fi
