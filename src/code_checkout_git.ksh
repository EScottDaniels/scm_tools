#!/usr/bin/env ksh

# Mnemonic:	code_checkout_git.ksh
# Abstract: Checkout a branch in git. If there isn't a branch in the local
#			repo already the remote is checked. If a matching branch is
#			found the uer is prompted to create a local tracking branch on
#			the remote. If no branch is found on the remote, then the user
#			is prompted to create a local branch.
#
# Date:		2015 sometime
# Author:	E. Scott Daniels
#-----------------------------------------------------------------------------------

function usage {
	echo "unrecognised option $1"
	echo "usage: $0 [-R remote-name] branch"
	exit 1
}

remote=origin

while [[ $1 == -* ]]
do
	case $1 in
		-R)	remote=$2; shift;;

		*)	usage;;
	esac

	shift
done

if [[ -z $1 ]]
then
	usage
	exit 1
fi


branch="$1"
if [[ $1 != "-"* ]]
then
	git branch -l -a >/tmp/PID$$.data
	if ! grep -q " $1" /tmp/PID$$.data		# either '  name' or '* name'
	then
		if grep -q "$remote/$1" /tmp/PID$$.data
		then
			rm -f /tmp/PID$$.data

			printf "local branch $1 does not exist; create tracking branch from $remote? [yN]"
			read ans
			if [[ $ans != "y" ]]
			then
				echo "nothing done"
				exit
			fi

			opt="-b"
			branch="$1 $remote/$1"
		else
			printf "new branch $1; create? [yN]"
			read ans
			if [[ $ans != "y" ]]
			then
				echo "nothing done"
				exit
			fi

			opt="-b"
		fi
	fi

	rm -f /tmp/PID$$.data
fi

set -e
git checkout $opt $branch
git stash list
echo ""
git remote show $remote | grep "$1.*push"

exit 0
