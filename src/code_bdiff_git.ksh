#!/usr/bin/env ksh
# Mnemonic:	code_bdiff_git
# Abstract:	branch differences between current branch and another branch (assumed to
#			have a common parent somewhere.  Runs git log this-branch other-branch to 
#			show what is in other branch and not yet merged with this branch.
#			
#		
# Date:		17 Mar 2015
# Author:	E. Scott Daniels
#
# Mod:
# ---------------------------------------------------------------------------------

function cleanup {
	rm -fr /tmp/PID$$.*
	exit ${1:-0}
}

# set p to be the path to the directory with .git
# set rp to be what is below .git to the current directory
# exit if no .git is found
function set_p
{
	typeset have_setup=0

	p=$PWD				# yes, p and rp are global
	rp=""				# relative path under the .git directory to cwd

	if [[ -e .git ]]			# short circuit ensuring rp is set
	then
		rp="${p##*/}"
		return
	fi

	while true
	do
		if [[ -z $p ]]
		then
			echo "is this a git environment???  cannot find .git in this (or a parent) directory  [FAIL]"
			exit 1
		fi
	
		rp="/${p##*/}$rp"
		p=${p%/*}

		if (( have_setup == 0 ))  &&  [[ -f $p/.code_setup ]]	# pull in setup if we find it along the way
		then
			. $csd/.code_setup
			have_setup=1
		fi

		if [[ -e $p/.git ]]			# short circuit ensuring rp is set
		then
			return
		fi
	done
}


function branch_info
{
	if (( use_b ))
	then
		git status -s -b | awk ' /no branch/ { next; } /^\#\#/ { $1 = ""; print; exit( 0 ) } '
	else
		git status | awk '/On branch/ { 
			if( index( $(NF), ".." ) )
				print $(NF); 
			else
				#printf( "%s..origin/%s\n", $(NF), $(NF) )
				printf( "%s\n", $(NF) )
			exit( 0 ) 
		}'
	fi
}

function usage
{
	echo "$argv0 other-branch"
	exit 1
}

function bdiff
{
	echo ""
	echo "commits applied to $branch2 not yet merged with $branch1"
	git log --name-status $branch1..$branch2 | awk \
	-v verbose=$verbose \
	-v long_c0mmit=$long_commit \
	'
	function show_commit()
	{
		shown++;

		if( long_commit ) {
			printf( "%s %30s %15s %3d %s\n", date, cur_commit, auth, adds + deletes + mods,  cm )
		} else {
			printf( "%s %s %15s %3d %s\n", date, substr( cur_commit, 1, 11 ), auth, adds + deletes + mods,  cm )
		}
		if( verbose ) {
			split( list, a, " " )
			for( x in a ) {
				gsub( ":", " ", a[x] )
				printf( "%88s %s\n", " ", a[x]);
			}

			printf( "\n" );
		}
	}

	NF < 2 { next; }
	/^commit/ { 
		if( cur_commit != "" ) {
			show_commit();
		}
		list = ""
		cm = ""; 
		cur_commit = $2; 
		adds = 0
		deletes = 0
		mods = 0
		next; 
	}
	/^Author:/ { 
			if( index( $2, "," ) )
				auth = substr( $2, 1, length( $2 ) -1 );
			else
				auth = $3
			auth = sprintf( "%10s", substr( auth, 1, 10 ) ); 
			next; 
	}
	/^Date:/ { date = sprintf( "%2s %3s %4s", $4, $3, $6 ); next; }

	cm == "" { 						# first commit comment/message
		cm = $0; 
		cmsg[cur_commit] = cm
		next; 
	}

	/^A/	{ list = list $1 ":" $2 " "; adds++; next; }
	/^D/	{ list = list $1 ":" $2 " "; deletes++; next; }
	/^M/	{ list = list $1 ":" $2 " "; mods++; next; }

	END {
		if( cur_commit != "" ) {
			show_commit()
		} else {
			if( ! shown )
				printf( "====none====\n" );
		}
	}
	'
}
# ------------------------------------------------------------------------------

set_p			# set p and rp paths looking for .git error if not found
this_branch=$( branch_info )

TMP="${TMP:-/tmp}"
argv0=${0##*/}
verbose=0
reverse=0
min_changes=2
short=0
shortn=10
commit_centric=0
ustring="[-c] [-m min-commit] [-r] [-s | -S n] [-v] other-branch"

long_commit=0
while [[ $1 == -* ]]
do
	case $1 in 
		-c)	commit_centric=1;;
		-l)	long_commit=1;;
		-m)	min_changes=$2; shift;;	
		-r)	reverse=1;;
		-s) short=1;;
		-S) short=1; shortn=$2; shift;;
		-v)	verbose=1;;

		--man)	show_man; cleanup 1;;
		-\?)	usage; cleanup 1;;
		-*)	error_msg "unrecognised option: $1"
			usage;
			cleanup 1
			;;
	esac

	shift
done

branch1=$1
branch2="${2:-$this_branch}"
bdiff

echo ""
branch1="${2:-$this_branch}"
branch2=$1
bdiff


cleanup 0
exit 0
