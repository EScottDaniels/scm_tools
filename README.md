          
 
 
[//]: # ( 
WARNING! This file is generated from an {X}fm source file. Do 
NOT modify it in place; modify the source and rebuild it 
) 
 
 
 
 
# SCM Tools 
This is a small collection of scripts which make managing code 
in a source code control (SCM) system a bit easier. These tools 
started in the late 1990s to manage CVS based code, and were 
extended to provide the same interface when using git. The CVS 
tools have been dropped (who uses CVS anymore, right?). 
 
The basic tools are described in the following paragraphs. 
Examples assume that shell aliases have been set up for the full 
script names (e.g. alias code_hist=code_hist_git.ksh). 
 
## code_stat_git..ksh 
Generates a consice status of the code in the current directory 
(or in the whole repo if -a is given on the command line). 
 
A sample output (below) shows that all of the source files are 
staged (added), but not yet committed. The licence and readme 
files have been committed, but not pushed to the remote. Further 
the readme has additional modifications which are not yet staged 
or committed (14 new lines). 
 
 
     
     
     
      $ code_stat         
      Branch: master..origin/master
       MODULE                          NEW   MOD STAGD PPEND   DEL  UnMERG   ADDS  DROPS
       LICENSE                                            X               
       README                                 X           X                    14      0
       src/code_aliases.ksh                         X                     
       src/code_bdiff_git.ksh                       X                     
       src/code_checkout_git.ksh                    X                     
       src/code_hist_git.ksh                        X                     
       src/code_stash_git.ksh                       X                     
       src/code_stat_git.ksh                        X                     
       src/code_synch_git.ksh                       X                     
 
 
 
 
## code_stash_git.ksh 
Provides an easy interface for staging (add), commiting, and 
pushing to a remote. 
 
 
## code_hist_git.ksh 
Provides a concise history of the repo, with the option to 
provide more detailed inforamtion when necessary. 
 
     
     
     
      # commit style history
      $ code_hist -S 10 -c
      6635f183 15 Dec 2019    Daniels     Another readme formatting attempt 
      a3992ccf 15 Dec 2019    Daniels     Attempt to get bb to not format readme 
      3d049d6a 11 Dec 2019    Daniels     Try to fix README so that bitbucket won't format 
      475a545c 11 Dec 2019    Daniels     Move readme back to original name 
      448f3dad 11 Dec 2019    Daniels     Rename readme 
      5603d228 11 Dec 2019    Daniels     Add scripts and update readme 
      3dd8a6b5 11 Dec 2019    Daniels     Initial commit -- license, readme 
        
      # verbose commit style history
      $ code_hist -v -S 1 -c
      19a05276 15 Dec 2019    Daniels     Convert readme to be {X}fm src 
            Makefile(A)
            README(M)
            readme.xfm(A)
        
      # module style history
      # code_hist README  
      scm_tools/README  5 total changes
         3dd8a6b5 A 11 Dec 2019    Daniels
         5603d228 M 11 Dec 2019    Daniels
         3d049d6a M 11 Dec 2019    Daniels
         a3992ccf M 15 Dec 2019    Daniels
         6635f183 M 15 Dec 2019    Daniels
        
 
 
 
 
## code_synch_git.ksh 
Allows the current repo to be rebased on top of a remote copy of 
the repo (upstream, origin, etc). 
