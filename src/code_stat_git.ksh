#!/usr/bin/env ksh
# Mnemonic:	code_stat_git  
# Abstract:	Generate status of things in a git working (sub)directory. By default
#			the current directory is all that is listed, and new files (files 
#			unknown to git) are not reported on.
#		
# Date:		03 March 2012
# Author:	E. Scott Daniels
#
# Note:		to revert changes to a file back to what they are in the checked in 
#		version use:
#			git checkout HEAD -- <filename>
#
# Mod:		08 Jul 2013 - To present unpushed information and to show new files
#				even if nothing changed in current directory. Limits now to 
#				current directory and supports -q option to possibly run in a more 
#				quiet manner when no changes exist. Major change was to use the 
#				--porcelain interface for getting state information. 
#			17 Sep 2013 - Support for better pruning and showing changes from just
#				subdirectories as well as adding an option to show all changes under
#				the directory with .git regardless of the pwd. 
#			11 Nov 2013 - fixed bug preventing deleted module from being marked in some cases.
#				added force update remote first option
#			27 Apr 2014 - Added back branch name on all listing. 
#			01 Feb 2015 - Finally fixed the unpushed things showing across branches.
# ---------------------------------------------------------------------------------

function cleanup {
	rm -f /tmp/PID$$.*
	exit ${1:-0}
}


# set p to be the path to the directory with .git
# set rp to be what is below .git to the current directory
# exit if no .git is found
function set_p
{
	typeset have_setup=0

	p=$PWD				# yes, p and rp are global
	rp=""				# relative path under the .git directory to cwd

	if [[ -e .git ]]			# short circuit ensuring rp is set
	then
		#rp="${p##*/}"
		rp=""				# no path; git seems to be dropping the leading directory name now
		return
	fi

	while true
	do
		if [[ -z $p ]]
		then
			echo "is this a git environment???  cannot find .git in this (or a parent) directory  [FAIL]"
			exit 1
		fi
	
		rp="/${p##*/}$rp"
		p=${p%/*}

		if (( have_setup == 0 ))  &&  [[ -f $p/.code_setup ]]	# pull in setup if we find it along the way
		then
			. $csd/.code_setup
			have_setup=1
		fi

		if [[ -e $p/.git ]]			# short circuit ensuring rp is set
		then
			rp="${rp#/}"			# lead slant needs to go
			return
		fi
	done
}

# dump a list of things that are commited locally, but not pushed to the remote repo
function get_unpushed
{
    git log ${branch%%..*} --name-only --oneline --not --remotes=origin -v | awk -v decend=$decend -v local=$local -v path="${rp}" '
		NF == 1 { 
			if( decend || !local || !path || index( $1, path ) == 1 ) 
			{
				if( path )
				{
					gsub( path "/", "", $1 );				# remove the path from the name
					if( ! decend && index( $1, "/" ) > 0 )
						next;
				}

				print "unpushed:", $1; 
			}
		}
	'
}


# run a no-execute push to see what would be pushed
function verify_push
{
	if (( test_push ))
	then
		git push -n -v 2>&1
	fi
}

#
# determine what hasn't been pushed 
#
function check_not_pushed
{
	git log --name-only >/tmp/PID$$.log
	verify_push | awk -v fname=/tmp/PID$$.log '

		/\.\./	{ 
			n = split( $1, a, "." );
			start[idx+0] = a[n];
			stop[idx+0] = a[1];
			idx++;
			next;
		}

		END {
			while( (getline <fname) > 0 )
			{
				if( $1 == "commit" )
				{
					if( !snarf )				# not snarfing, check to see if we should start
					{
						for( i = 0; i < idx; i++ )
						{
							if( index( $0, start[i] ) )
							{
								snarf = 1;
								break;
							}
						}
					}
					else
					{
						if( snarf > 0 )			
						{
							for( i = 0; i < idx; i++ )
							{
								if( index( $0, stop[i] ) )
								{
									snarf = 0;		
									break;
								}
							}
						}
					}
				}
				else
					if( snarf  &&  NF == 1 )
					{
						if( header++ < 1 )
							printf( "not pushed:\n" );
						printf( "\t%s\n",  $0 );
					}
			}

			close( fname );
		}
	'
	rm /tmp/PID$$.log
}


# generate a list of things that have changed on the remote (after a fetch)
function merge_stat
{
	if (( no_merge_stat ))
	then
		return
	fi

	# this will toss an error if the remote is bare, so toss the error out
	git diff --name-status ${1:-master...origin/master} ${2} 2>/dev/null | sed 's/^/merge: /'
}

function branch_info
{
	if (( ! show_branch ))
	then
		echo ""
		return
	fi

	if (( use_b ))
	then
		git status -s -b | awk ' /no branch/ { next; } /^\#\#/ { $1 = ""; print; exit( 0 ) } '
	else
		git status | awk '/On branch/ { 
			if( index( $(NF), ".." ) )
				print $(NF); 
			else
				printf( "%s..origin/%s\n", $(NF), $(NF) )
			exit( 0 ) 
		}'
	fi
}

function usage
{
	cat <<endKat
	Generate stats on the git files in the current directory.

	usage: $argv0 [-A] [-a | -b] [-L] [-M] [-n] [-N] [-p] [-P] [-q] [-S] [-v]
	-a -- decend into subdirectories (show all)
	-A -- show all (including files not committed)
	-n -- do not show merge/unmerge status information

	The following options cause a class of file to be listed (no formatting)
	-D -- delted files
	-N -- new flies
	-S -- staged files
	-M -- modified files
	-P -- pending files (not pushed to remote)
	
	The following options exist for compatability with CVS code* scripts and
	are ignored:
		-c, -d, -h, -l, -m
endKat
	exit 1
}
# ------------------------------------------------------------------------------

set_p			# set p and pr paths looking for .git error if not found

# git may maintain this info in the directory meaning we don't need it :)
githost=${GITHOST:-localhost}
gitroot=${GITROOT:-$USER@$githost:/usr2/git_root}
export GIT_RSH=${GIT_RSH:-ssh}						# git defaults to this so we probably don't need it

TMP="${TMP:-/tmp}"
argv0=$0
verbose=0
ustring=""
cmd=status
flags=""
short_list=1				# short (matrix) list on by default
test_pushed=1
quiet="-uall"				# causes everything to be listed in new directories (-q turns off)
no_merge_stat=0
local=1					# show info about cwd only
decend=0
show_branch=1
force_update=0
drop_new=1

show="all"			# -N, -M, -P, -S change this

while [[ $1 == -* ]]
do
	case $1 in 
		-c)	;;					# ignored compat with cvs version
		-d) ;;
		-h)	;;
		-l)	;;
		-m)	;;					

		-a)	decend=1; local=0;;
		-A)	drop_new=0;;
		-b)	decend=1;;
		-D)	show="del";;					# these are mostly for code_stash and that  cannot handle branch info in output

		-L)	short_list=0;;
		-M)	show_branch=0; show="mod";;		# these are mostly for code_stash and that  cannot handle branch info in output
		-N)	show_branch=0; show="new";;
		-n)	no_merge_stat=1;;
		-p)	test_pushed=0;;
		-P)	show_branch=0; show="ppending";;
		-q)	quiet="";;
		-S)	show_branch=0; show="staged";;
		
		-u)	force_update=1;;
	
		-v)	
			verbose=1
			;;

		--man)	show_man; cleanup 1;;
		-\?)	usage; cleanup 1;;
		-*)	error_msg "unrecognised option: $1"
			usage;
			cleanup 1
			;;
	esac

	shift
done


branch=$( branch_info )
if (( show_branch ))
then
	echo "Branch: $branch"
fi

if (( force_update ))
then
	echo "forcing an update from remote tracking branch..."
	git remote update
fi


(
	if (( !local )) 
	then
		status_opts="--porcelain"		# paths are ok for -a 
	else
		status_opts="--short"  			# --porclain paths are from the root rather than relative (e.g. no ../foo)
	fi

	git diff --numstat  | sed 's/^/diff: /'		# must be first
	merge_stat 
	get_unpushed 
	git status $quiet $status_opts ) | awk \
	-v short_list=$short_list \
	-v path="${rp}/" \
	-v show="$show" \
	-v local=$local \
	-v decend=$decend \
	-v drop_new=$drop_new \
	-v branch_info="${branch:-master...origin/master}" \
	-v remote_info="${remote:-none}" \
	-v verbose=${verbose:-0} \
	'
	BEGIN {
		unknown = -1;
		unset = 0;		# states
		mod = 1;
		new = 2;
		ready = 3;		# staged, ready for commit
		merge = 4;
		deleted = 5;
		unpushed = 6;	# committed but not pushed to remote
		unmerged = 9;

		state = unset;

		pl = length( path );
		np = split( path, a, "/" );
	}
	
	# track state of the filename (with)
	function update( what, with, marker,		i, a )
	{
		if( drop_new && what == new ) 
		{
			if( index( with, "." )  )
			{
				split( with,  a, "." )
				dtype[a[2]]++
			}

			return;
		}

		i = idx[what]++;
		fname[what,i] = with;
		matrix[with,what] = marker;
		seen[with] = 1;
		if( length( with ) > max_size )
			max_size = length( with );
	}

	# return the file name if we should keep using it, "" otherwise
	# git status doesnt give relative information 
	function ok2keep( what, 		pl )
	{
		# assume what is something like dir/dir/dir/file

		if( !local || path == "/" )
			return what;

		if( index( what, "/" ) <= 0 )
			return what;
		pl = length( path );

		if( decend || split( what, a, "/" ) == np )		# could match 
		{
			if( decend || ! index( substr( what, pl+1 ), "/" ) )		# no / after the length of path
			{
				if( substr( what, 1, pl ) == path )
				{
						what = substr( what, pl+1 );
						if( index( what, "/" ) )
							return "";
						else
							return what;
				}
				else
					if( decend )
						return what;
			}
		}

		return "";
	}

	function list( type, 	n )
	{
		for( n in seen )
			if( matrix[n,type] )
				printf( "%s ", n );
		printf( "\n" );
	}

	# maybe not short, but a matrix
	function show_short()
	{
		fmt = sprintf( "%%-%ds", max_size+4 );
		if( show == "all" )
		{
			#gsub( "#", "", branch_info );
			#printf( "\nBranch: %s\n", branch_info );	
			#printf( "Remote: %s\n", remote_info );	
			printf( "\n" )
			printf( fmt " %5s %5s %5s %5s %5s %7s %6s %6s\n", "MODULE", "NEW", "MOD", "STAGD", "PPEND", "DEL", "UnMERG", "ADDS", "DROPS" );
			for( n in seen )
			{
				saw_something = 1			# some awk versions (old) dont support length( array ), so this is needed
				printf( fmt, n ) >"sort.in"
				printf( "%5s ", matrix[n,new] ?  "X" : " " ) >"sort.in"
				printf( "%5s ", matrix[n,mod] ?  (matrix[n,merge] == 1 ? "M" : "X") : " " ) >"sort.in"
				printf( "%5s ", matrix[n,ready] ?  "X" : " " ) >"sort.in"
				printf( "%5s ", matrix[n,unpushed] ?  "X" : " " ) >"sort.in"
				printf( "%5s ", matrix[n,deleted] ?  "X" : " " ) >"sort.in"
				printf( "%7s ", matrix[n,unmerged] ?  matrix[n,unmerged] : " " ) >"sort.in"

				if( matrix[n,mod]  ) {
					printf( " %6d %6d", adds[n], drops[n] ) >"sort.in"
				}

				#if( genc && matrix[n,mod] )
				#	printf( "\tcode_diff_git.ksh %s|more\n", n ) >"sort.in";
				#else
					printf( "\n" ) >"sort.in"
			}
	
			if( saw_something )
			{
				close( "sort.in" );
				system( "sort sort.in; rm sort.in" );

				if( genc )
				{
					printf( "\n" );
					for( n in seen )
					{
						if( matrix[n,unmerged] )
							printf( "git diff --color %s -- %s|more\n", branch_info, n );
						else
						if( matrix[n,mod] )
							printf( "\tcode_diff_git.ksh %s|more\n", n );
					}
				}
			}

			if( drop_new )
			{
				count = 0;
				for( x in dtype ) {
					count++;			# length( array) not supported in all awks
				}

				if( verbose && count > 0 ) {
					printf( "\nNew files not listed above:\n" )
					for( x in dtype )
						printf( "\t%-10s %4d files\n", x, dtype[x] )
				} else {
					printf( "\n%d new files were omitted from the list above.\n", count )
				}
			}
		}
		else		# write simple lists of certain types
		{
			if( show == "staged" )
				list( ready );
			else
			if( show == "ppending" )
				list( unpushed );
			else
			if( show == "mod" )
				list( mod );
			else
			if( show == "new" )
				list( new );
			else
			if( show == "del" )
				list( deleted );
			else
				printf( "unrecognised show type: %s\n", show ) >"/dev/fd/2";
		}
	}

	
	function show_long()
	{
		printf( "Branch: %s\n", branch );

		if( idx[new]+0 > 0 )
		{
			printf( "New Files:\n" );
			for( i = 0; i < idx[new]; i++ )
				printf( "\t%s\n", fname[new,i] );
			printf( "\n" );
		}

		if( idx[mod]+0 > 0 )
		{
			printf( "Modified Files:\n" );
			for( i = 0; i < idx[mod]; i++ )
				printf( "\t%s\n", fname[mod,i] );
			printf( "\n" );
		}

		if( idx[ready]+0 > 0 )
		{
			printf( "Ready For Commit:\n" );
			for( i = 0; i < idx[ready]; i++ )
				printf( "\t%s\n", fname[ready,i] );
			printf( "\n" );
		}

		if( idx[deleted]+0 )
		{
			printf( "Deleted:\n" );
			for( i = 0; i < idx[deleted]; i++ )
				printf( "\t%s\n", fname[deleted,i] );
			printf( "\n" );
		}
	}
	# ------------------------------------------------------------------------------------

	/^diff:/ {
		dfn = ok2keep( $NF );
		if( dfn != "" ) {
			adds[dfn] = $2;
			drops[dfn] = $3;
		}
		next;
	}

	/unpushed:/ { 					# file committed locally but not pusehd to remote
		if( ok2keep( $2 ) == "" ) 
			next;

		upcount++;
		branch = "unpushed";
		state = unpushed;
		matrix[$NF,state] = 1;
		i = idx[state]++;
		fname[state,i] = $NF;
		matrix[$NF,state] = 1;
		seen[$NF] = 1;
		if( length( $(NF) ) > max_size )
			max_size = length( $NF );
		state = unset;
		next;
	}

	/merge:/ { 
		if( ok2keep( $2 ) != "" ) 
			update( unmerged, $3, $2 ); 
		next; 
	}

	{
		xystate = substr( $0, 1, 2 );
		xstate = substr( xystate, 1, 1 );
		ystate = substr( xystate, 2, 1 );
		state = unknown;

		if( ! (filename = ok2keep( $2 )) )
			next;
	}


	xstate == "?"  && ystate == "?" { update( new, filename, "X" ); next;  }

	xstate == "D" && ystate == "D" { update( deleted, filename, "X" ); update( unmerged, filename, "both" ); next;  }
	xstate == "D" && ystate == " " { update( deleted, filename, "X" ); update( unmerged, filename, "us" ); next;  }
	xstate == "D" && ystate == "U" { update( deleted, filename,"X" ); update( unmerged, filename, "us" ); next;  }

	xstate == "A" && ystate == "U" { update( ready, filename, "X" ); update( unmerged, filename, "us" ); next;  }
	xstate == "U" && ystate == "D" { update( deleted, filename, "X" ); update( unmerged, filename, "them" ); next;  }
	xstate == "U" && ystate == "A" { update( ready, filename,"X" ); update( unmerged, filename, "them" ); next;  }
	xstate == "A" && ystate == "A" { update( ready, filename,"X" ); update( unmerged, filename, "both" ); next;  }
	xstate == "U" && ystate == "U" { update( mod, filename,"X" ); update( unmerged, filename, "both" ); next;  }

	xstate == "M"                  { update( ready, filename, "X" ); next; }
	#xstate == "R"                  { update( mod, filename, "R" ); next; }		# renamed file
	xstate == " " && ystate == "M" { update( mod, filename, "X" ); next; }
	xstate == "A"                  { update( ready, filename, "X" ); next; }
	xstate == "D" || ystate == "D"{ update( deleted, filename, "" );  }

	#xstate == "?"  && ystate == "?" { update( new, filename );  }
	#xstate == "A" || ystate == "A"{ update( ready, filename );  }
	#xstate == "D" || ystate == "D"{ update( deleted, filename );  }
	#xstate == "M" || ystate == "M"{ update( mod, filename );  }
	#xstate == "U" || ystate == "U"{ update( merge, filename );  }

	END {
		if( short_list )
			show_short();
		else
			show_long();
	}
	
'

if (( verbose ))
then
	echo ""
	echo "Remote info:"
	git remote -v
fi


cleanup $?

exit
# from man page:
# X          Y     Meaning
# -------------------------------------------------
#           [MD]   not updated
# M        [ MD]   updated in index
# A        [ MD]   added to index
# D         [ M]   deleted from index
# R        [ MD]   renamed in index
# C        [ MD]   copied in index
# [MARC]           index and work tree matches
# [ MARC]     M    work tree changed since index
# [ MARC]     D    deleted in work tree
# -------------------------------------------------
# D           D    unmerged, both deleted
# A           U    unmerged, added by us
# U           D    unmerged, deleted by them
# U           A    unmerged, added by them
# D           U    unmerged, deleted by us
# D                deleted by us
# A           A    unmerged, both added
# U           U    unmerged, both modified
# -------------------------------------------------
# ?           ?    untracked
# -------------------------------------------------
