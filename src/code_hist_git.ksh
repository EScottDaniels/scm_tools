#!/usr/bin/env ksh
# Mnemonic:	code_hist_git - generate a history
# Abstract:	Accepts zero or more module names (assumed to be in the current dir) and 
#			presents a list of commits in which the module(s) were modified (added, deleted,
#			etc.).  If no module names are given, then all modultes are listed. The default
#			is to list only modules that have been involved in more than one commit (so 
#			as not to list every bloody add when the module hasn't been changed since the 
#			initial commit.  Use -m n to change the minimum commit value if needed. 
#		
# Date:		04 June 2014
# Author:	E. Scott Daniels
#
# Mod:		02 Apr 2015 - Refined commit centric output.
#			06 Jan 2017 - Refined author column in commit centric output.
# ---------------------------------------------------------------------------------

function cleanup {
	rm -f /tmp/PID$$.*
	exit ${1:-0}
}

# set p to be the path to the directory with .git
# set rp to be what is below .git to the current directory
# exit if no .git is found
function set_p
{
	typeset have_setup=0

	p=$PWD				# yes, p and rp are global
	rp=""				# relative path under the .git directory to cwd

	if [[ -e .git ]]			# short circuit ensuring rp is set
	then
		rp="${p##*/}"
		return
	fi

	while true
	do
		if [[ -z $p ]]
		then
			echo "is this a git environment???  cannot find .git in this (or a parent) directory  [FAIL]"
			exit 1
		fi
	
		rp="/${p##*/}$rp"
		p=${p%/*}

		if (( have_setup == 0 ))  &&  [[ -f $p/.code_setup ]]	# pull in setup if we find it along the way
		then
			. $csd/.code_setup
			have_setup=1
		fi

		if [[ -e $p/.git ]]			# short circuit ensuring rp is set
		then
			return
		fi
	done
}


function branch_info
{
	if (( use_b ))
	then
		git status -s -b | awk ' /no branch/ { next; } /^\#\#/ { $1 = ""; print; exit( 0 ) } '
	else
		git status | awk '/On branch/ { 
			if( index( $(NF), ".." ) )
				print $(NF); 
			else
				printf( "%s..origin/%s\n", $(NF), $(NF) )
			exit( 0 ) 
		}'
	fi
}

function usage
{
	echo "$argv0 [-A] [-a] [-c] [-F] [-l | -L n] [-m n] [-M] [-r] [-s | -S n] [-v] [module-name(s)]"
	cat <<-endKat
	-A  - Show email addresses rather than names
	-a  - show all changes, not just module names listed
	-c  - commit oriented list rather than module oriented list
	-F  - Show full author name, not just last token in author name
	-l  - show long commit id
	-L  - show n characters of commit id rther than first 8
	-m  - Show modules only if they have n changes (default is 2)
	-M  - Show merge commits too
	-s  - Show short list of last 10 commits
	-S  - Show last n commits
	-v  - verbose (lists changed module for each commit if -c given)
endKat
	exit 1
}
# ------------------------------------------------------------------------------

set_p			# set p and rp paths looking for .git error if not found

# git may maintain this info in the directory meaning we don't need it :)
#githost=${GITHOST:-localhost}
#gitroot=${GITROOT:-$USER@$githost:/usr2/git_root}
#export GIT_RSH=${GIT_RSH:-ssh}						# git defaults to this so we probably don't need it

TMP="${TMP:-/tmp}"
argv0=${0##*/}
verbose=0
reverse=0
min_changes=2
short=0
long_cid=0
shortn=9e9
commit_centric=0
show_all=0
cid_size=8
stats_only=0
keep_merge=0
show_mail=0					# -A turns on to show mail address instead of name
full_name=0					# -F turns on to show full name rather than last token
ustring="[-c] [-m min-commit] [-r] [-s | -S n] [-v] [module-name(s)]"

while [[ $1 == -* ]]
do
	case $1 in 
		-A)	show_mail=1;;
		-a)	show_all=1;;
		-c)	commit_centric=1;;
		-C)	stats_only=1;;
		-F)	full_name=1;;
		-l) long_cid=1;;
		-L)	long_cid=0; cid_size=$2; shift;;
		-m)	min_changes=$2; shift;;	
		-M)	keep_merge=1;;
		-r)	reverse=1;;
		-s) short=1; shortn=10;;
		-S) short=1; shortn=$2; shift;;
		-v)	verbose=1;;

		--man)	show_man; cleanup 1;;
		-\?)	usage; cleanup 1;;
		-*)	error_msg "unrecognised option: $1"
			usage;
			cleanup 1
			;;
	esac

	shift
done


what="$@"
(
	list_git_tags.ksh
	git log --name-status 
)| awk \
	-v stats_only=$stats_only \
	-v keep_merge=$keep_merge \
	-v full_name=$full_name \
	-v show_all=$show_all \
	-v show_mail=$show_mail \
	-v full_name=$full_name \
	-v min_changes=${min_changes:-2} \
	-v verbose="${verbose:-0}" \
	-v reverse=${reverse:-0} \
	-v rel_path="${rp#/}" \
	-v short=$short \
	-v shortn=$shortn \
	-v short_cid=$short_cid \
	-v commit_centric=$commit_centric \
	-v what="$what" \
	-v long_cid=$long_cid \
	-v cid_size=$cid_size \
	'	###

	BEGIN {
		name_len = 10;
	}
	function last_ele( s, 		a, n )
	{
return s;
		if( show_all ) {
			return s;
		}

		n = split( s, a, "/" )
		return a[n];
	}

	NF < 2 { next; }

	/^tag: / {
		tag_map[$3] = $2
		next
	}

	/^commit/ { 
		cm = ""; 
		if( long_cid ) {
			cur_commit = $2; 
		} else {
			cur_commit = substr( $2, 1, cid_size ); 
		}

		if( cinput[cur_commit]++ == 0 ) {
			long_commit[cur_commit] = $2
			corder[cidx++] = cur_commit;
		}
		next; 
	}

	/^Merge: / { 
			mtag[cur_commit] = "(Merge)";
			next;
	}

	/^Author:/ { 
			if( show_mail ) {
				auth = substr( $(NF), 2, length( $(NF) ) - 2  )
			} else {
				if( NF > 2 ) {
					if( full_name || length( $(NF-1) ) <= 2  ) {	# short last names get banged in with first names
						auth = ""
						for( i = 2; i < NF; i++ ) {
							auth = auth " " $(i)
						}
					} else {
						auth = $(NF-1)
					}
				} else {
					auth = $NF				# assume just "author mail" tokens
				}
	
				if( substr( auth, length( auth), 1 ) == "," ) {
					auth = substr( auth, 1, length( auth ) -1 );
				} else {
					if( index( auth, "@" ) ) {
						split( auth, a, "@" )
						auth = a[1]
	
						if( substr( auth, 1, 1 ) == "<" ) {
							auth = substr( auth, 2 )
						}
					}
				}
	
				if( ! full_name ) {
					nlba = sprintf( "%-10s", substr( auth, 1, 10 ) ); 
					auth = sprintf( "%10s", substr( auth, 1, 10 ) ); 
					nlb_auth[auth] = nlba
				} else {
					nlba = sprintf( "%-25s", substr( auth, 1, 25 ) ); 
					auth = sprintf( "%25s", substr( auth, 1, 25 ) ); 
					nlb_auth[auth] = nlba
				}
			}
			
			if( (cidx < shortn) && (length( auth ) > name_len) ) {
				name_len = length( auth );	
			}
			author[cur_commit] = auth;
			count_author[auth]++;
			next; 
	}
	/^Date:/ { 
		date = sprintf( "%2s %3s %4s", $4, $3, $6 ); 
		cdate[cur_commit] = date
		count_year[$6]++
		count_month[ $6 " " $3]++
		next; 
	}

	cm == "" { 					# snarf only the first line of the message
		cm = $0; 
		cmsg[cur_commit] = cm
		next; 
	}

	/^A/	{ 
		if( reverse )
			clist[$2] = clist[$2] cur_commit " A " date " " auth ";";
		else
			clist[$2] = cur_commit " A " date " " auth ";" clist[$2];
		if( last[$2] == "" )
		{
			all_list = all_list last_ele( $2 ) " ";
			last[$2] = date;
			cmhi[$2] = 0
		}
		cmhist[$2,cmhi[$2]] = cm;
		cmhi[$2]++;
		mlist[cur_commit] = mlist[cur_commit] $2 "(A) ";
		next;
	}
	/^D/	{ 
		if( reverse )
			clist[$2] = clist[$2] cur_commit " D " date " " auth ";";
		else
			clist[$2] = cur_commit " D " date " " auth ";" clist[$2];
		if( last[$2] == "" )
		{
			all_list = all_list last_ele( $2 ) " ";
			last[$2] = date;
			cmhi[$2] = 0
		}
		cmhist[$2,cmhi[$2]] = cm;
		cmhi[$2]++;
		mlist[cur_commit] = mlist[cur_commit] $2 "(D) ";
		next;
	}
	/^M/	{ 
		if( last[$2] == "" )
		{
			all_list = all_list last_ele( $2 ) " ";
			last[$2] = date;
			cmhi[$2] = 0
		}
		if( reverse )
			clist[$2] = clist[$2] cur_commit " M " date " " auth ";";
		else
			clist[$2] = cur_commit " M " date " " auth ";" clist[$2];
		cmhist[$2,cmhi[$2]] = cm;
		cmhi[$2]++;
		mlist[cur_commit] = mlist[cur_commit] $2 "(M) ";
		next;
	}

	END {
		if( stats_only ) {
			scmd = "sort -r -k 1n,1" 
			for( y in count_year ) { 
				printf( "%d %s\n", y, count_year[y] ) | scmd
			}
			close( scmd );

			printf( "\n" )
			scmd = "sort -r -k 1r,1 -k 2M,2" 
			for( ym in count_month ) {
				printf "%s %d\n", ym, count_month[ym]  | scmd
			}
			close( scmd );

			printf( "\n" )
			scmd = "sort -k 2rn,2 -k 1,1"
			for( ac in count_author ) {
				printf( "%s %d\n", nlb_auth[ac], count_author[ac] ) | scmd 
			}
			close( scmd )

			exit( 0 )
		}
		
		if( show_all || what == "" ) {			# if no specific module given, or all requested
			what = all_list;
		}

		fmt_str = sprintf( "%%s %%s %%16s %%-%ds %%s %%s\n", name_len );
		if( commit_centric ) {
			todo = short ? (shortn > cidx ? cidx : shortn) : cidx
			for( j = 0; j < todo; j++ ) {
				if( keep_merge || mtag[corder[j]] == "" ) {
					lcid = long_commit[corder[j]]				# tags are mapped with long commit id only
					tag = tag_map[lcid]
					printf( fmt_str, corder[j], cdate[corder[j]], tag, author[corder[j]], cmsg[corder[j]], mtag[corder[j]] );
					if( verbose ) {
						split( mlist[corder[j]], b, " " )
						need_space = 0
						for( n in b ) {
							need_space = 1;
							printf( "\t%s\n", b[n] )
						}
	
						if( need_space ) {
							printf( "\n" );
						}
					}
				}
			}
		} else {
			n = split( what, a, " " );
			for( i =1; i <= n; i++ )
			{
				module = rel_path "/" a[i];
				m = split( clist[module], b, ";" );
				if( m == 0 ) {
					m = split( clist[a[i]], b, ";" );
				}
				if( clist[m] == "" ) {
					m--;
				}
				todo = short > 0 ? shortn : 9000000;
				if( m >= min_changes )
				{
					printf( "%s  %d total changes\n", module, m )
					for( j = 1; j <= m && todo > 0; j++ )
					{
						if( verbose )
						{
							if( reverse ) {
								printf( "\t%s  %s\n", b[j], cmhist[module,j-1] );
	
							} else {
								printf( "\t%s  %s\n", b[j], cmhist[module,cmhi[module]-j] );
							}
						}
						else
							printf( "\t%s\n", b[j] );
						todo--;
					}
					if( shortn ) {
						printf( "\n" );
					}
				}

			}
		}

		printf( "\n" );
	}
'

cleanup 0
exit 0

from git log
commit 61b032af89133d59ca108c4b55b235a8a36f32bc
Author: Scott Daniels <daniels@research.att.com>
Date:   Thu May 22 08:26:45 2014 -0400

    Corrected bug that was causing tier to be missed in the data.

A       auto/dc_volte_rpt.ksh

commit b226a3541867b9c6f57605d2be95c8f7df0b2a5f
Author: Scott Daniels <daniels@research.att.com>
Date:   Wed Apr 2 08:34:26 2014 -0400

    corrected target install list and added probe.min to the nuke list

M       makefile
M       makefile.global
M       probe

