#!/usr/bin/env ksh 
# Mnemonic:	stash -- put something into the repository (git oriented)
# Abstract:		git is a three stage model as far as the remote (public)
#				repository is concerned:
#					modified-src -> stage -> commit -> push
#	
#				By default, stash does a commit of the source and 
#				will push only if -p is on.  It will only stage the 
#				module(s) if -s	is given on the command line. Module
#				paths are relitive to the present directory.
#
#			Examples
#				alias code_stash="code_stash_git.ksh"
#				code_stash -N cool_prog.c		# commit a new module
#				code_stash -N -s cool_prog.c	# just stage a new prog
#				code_stash -s lib.c				# stash a module that has been modified
#				code_stash -push				# push; respond "staged" when prompted
#				code_stash --push cool_prog.c lib.c # push specifc modules which are staged
#				cose_stash -P					# push into a gerrit environment
#		
# Date:		04 March 2012
# Author:	E. Scott Daniels
#
# Mod:		08 Jul 2013 - allow branch/remote to be supplied on command line; doc changes
#			11 Nov 2013 - added ability to stash all deleted modules.
#			16 Aug 2016 - Now defaults to the current branch.
# ---------------------------------------------------------------------------------

function cleanup {
	rm -f /tmp/PID$$.*
	exit ${1:-0}
}


#  Given a branch name, ensure that it exists as a remote branch
function confirm_gb {
	git branch -l -a | grep remote | grep "/${1:-kad43086}"'$'
	git branch -l -a | grep remote | grep -q "/${1:-kad43086}"'$'
	if (( $? == 0 ))
	then
		return 0
	fi

	return 1
}

# set p to be the path to the directory with .git
# set rp to be what is below .git to the current directory
# exit if no .git is found
function set_p {
	typeset have_setup=0

	p=$PWD				# yes, p and rp are global
	rp=""				# relative path under the .git directory to cwd

	if [[ -e .git ]]			# short circuit ensuring rp is set
	then
		rp="${p##*/}"
		return
	fi

	while true
	do
		if [[ -z $p ]]
		then
			echo "is this a git environment???  cannot find .git in this (or a parent) directory  [FAIL]"
			exit 1
		fi
	
		rp="/${p##*/}$rp"
		p=${p%/*}

		if (( have_setup == 0 ))  &&  [[ -f $p/.code_setup ]]	# pull in setup if we find it along the way
		then
			. $csd/.code_setup
			have_setup=1
		fi

		if [[ -e $p/.git ]]			# short circuit ensuring rp is set
		then
			return
		fi
	done
}

#  dig the current branch out of git; writes to stdout
function get_branch {
	git branch -l | awk '/^[*] / { print $2; exit( 0 ) }'
}

function usage
{
	#echo "usage: $argv0 [-a] [-b branch] [-N | -I] [-n] [-r] [-m message] [-r remote] [-v]  source-file [source-file...]"
	echo "usage: $argv0 [-b branch] [-s|-p] [-m message | -M]  [-n] [-N] [-r remote-name] source-file [file...]"
	echo " "
	echo "	-s causes modules to be staged rather than committed "
	echo "	-p causes modules to be committed and then pushed to the remote repository"
	echo "  -M disables the short message (-m) and/or prompt for short message and causes git to invoke the editor for a long change message to be entered"
	#echo "	-I set import mode"
	#echo "	-N set new mode, use with -a for add only, else it adds and stashes"
}

# ------------------------------------------------------------------

set_p			# look for .git in the current dir or up the tree; bail if not found. leaves p set to the proper parent

argv0="${0##*/}"
date=""
tag_name=""
forreal=""
verbose=0
cmd="commit -s"						# force to add signed off by
msg="stashed on $(date) by $USER "
smsg_ok=${CODE_STASH_SMSGOK:-1}		# -M sets this to 0 which forces git to invoke the editor for a message (mostly for openstack things)
force_msg=${CODE_STASH_FMSG:-1}
new=0
umsg=""
remote="origin"
branch=$( get_branch )
git_opts=
trace_on="set -x"			# disabled if -n is given

push=0					# by default it is a local stash and not remote
gerrit_push=0
gerrit_branch=""		# for -P we need a gerrit branch and we don't default to anything!
require_gb=0			# if -P given and no -B, we'll prompt if this is set

ustring="[-b branch] [-N] [-n] [-m msg | -M] [-p] [-r remote]  [-v] [component] file [file ...]"

# must support all of the options that code_stash (cvs) supports; we might ignore them, but we cannot bail on them either
while [[ $1 == -* ]]
do
	case $1 in 
		-A)	author="--author=$2"; shift;;
		-a)	git_opts+=" --amend"
			cmd="commit "					# in amend mode, drop the signed off by
			;;

		-b)	gerrit_branch=$2; branch=$2; shift;;
		-m)	usrmsg="$2"; shift;;
		-M)	smsg_ok=0;;

		-n)	forreal="echo would execute:"
			trace_on=""
			;;

		-N)	new=1;;

		--push) push=1;;
		-p) push=1 ;;

		-r)	remote=$2; shift;;
		-s)	cmd=add;;					# stage
		-S)	cmd="commit "				# drop signed off by

		# -----  extensions to support bloody garret
		-B)	gerrit_branch="$2"; shift;;			# will prompt if not supplied when -P is given

		-P)	gerrit_push=1; 
			require_gb=1; 
			push=1
			;;

		-R)	reviewer="$2"; shift;;
		-W) wip="%wip";;			# mark the change as work in progress (only used if -P given)
		-WR) wip="%ready";;		# mark the change ready (only used if -P given)

		# ---- these support code_stash model for cvs and are ignored
		-a)	;;  #add_only=1;;
		-I)	;; 	#import=1; new=0;;
		#-r)	revision="-r $2"; shift;;
		# ------------------------------------------------------
		
		-v)	verbose=1;;
		#--man)	show_man; cleanup 1;;
		-\?)	usage; cleanup 1
			;;
			
		-*)	error_msg "unrecognised option: $1"
			usage

			cleanup 1
			;;
	esac

	shift
done

if [[ -z $branch ]]
then
	echo "[FAIL] can't determine branch with 'git branch -l'"
	echo "[INFO] if this is the first commit to the repo, use '-b <branch-name>' to force the name"
	exit 1 
fi


if (( require_gb )) && [[ -z $gerrit_branch ]]
then
	good_branch=0
	while (( ! good_branch ))
	do
		ans=""
		while [[ -z $ans ]]
		do
			printf -- "-P requires gerrit branch, enter branch [? for list]: "
			read ans
		done

		if [[ $ans == "?" ]]
		then
			git branch -l -a
		else
			if confirm_gb $ans
			then
				good_branch=1
				gerrit_branch="$ans"
			else
				echo "branch '$ans' doesn't seem to be valid"
			fi
		fi
	done
fi

what="$@"

if (( smsg_ok > 0 ))
then
	if (( push < 1 )) || [[ -n $what ]]
	then
		if [[ -n $forreal ]]
		then
			echo "no exec mode, message entered will be ignored"
		fi
		if [[ $cmd == "commit"*  ]]   && (( force_msg > 0 )) 
		then
			printf "enter stash message (blank for git to prompt): "		# blank will cause git to prompt
			read usrmsg
			if [[ -n $usrmsg ]]
			then
				usrmsg="$usrmsg"
				um_opt="-m"
			else
				smsg_ok=0					# force a pormpt
			fi
			msg=""
		fi
	fi
fi

if [[ -d $1 ]] 
then
	echo "ERR: git doesn't need to add directories"
	cleanup 0
fi


if (( push ))  && [[ $cmd == "add" ]]
then
	echo "ERR: cannot push with -s option -- no action taken [FAIL]"
	cleanup 1
fi

recursive_stash=0
if [[ -z $what ]] && (( push < 1 ))		# push with empty list just pushes the pending files
then									# otherwise prompt for what is to be shoved
	ans=""
	while [[ -z $ans ]]
	do
		echo "use capital (e.g. Mod) to stash from all directories, not just current"
		printf "stash ALL of what type of files? [new/mod/staged/deleted/list/recursive/abort/help]: "
		read ans

		case $ans in 
			h*)	cat <<-endKat

				new       -- stash all files that are new
				mod       -- stash all files that are modified
				deleted   -- stash all delete changes (commit the remove)
				staged    -- stash files that have been staged (current dir only)
				recursive -- stash all staged files including lower directories	

				list     -- list files with state indication
				abort    -- do nothing and exit


				endKat

				ans=""
				;;


			d*)
				what=$( code_stat_git.ksh -D )
				echo "stashing modified: $what"
				;;

			l*)
				code_stat_git.ksh
				ans=""
				;;

			M*)
				what=$( code_stat_git.ksh -M -a )
				echo "stashing all modified: $what"
				;;

			m*)
				what=$( code_stat_git.ksh -M )
				echo "stashing modified: $what"
				;;

			n*)
				new=1
				what=$( code_stat_git.ksh -N )
				echo "stashing new: $what"
				;;

			r*)
				what=""
				echo "stashing all staged files with recursion"
				recursive_stash=1
				;;

			s*)
				what=$( code_stat_git.ksh -S )
				echo "stashing staged: $what"
				;;

			S*)
				what=$( code_stat_git.ksh -S -a )
				echo "stashing all staged: $what"
				;;


			a*)
				echo "WARN: user terminated: nothing done"
				cleanup 0
				;;

			*)	ans="";;
		esac
	done

	if (( recursive_stash < 1))  &&  [[ -z $what ]]		# bail now to prevent accidental push
	then
		exho "INFO: nothing to be done  [OK]"
		cleanup 0
	fi
fi


if (( recursive_stash ))  ||  [[ -n $what ]]		# stash what is on the command line first
then
	if (( new > 0 )) || [[ $cmd == "add" ]]			# new files must be added if command isn't add, and add if command is add
	then
		echo "INFO: adding new module: $what"
		$forreal git add ${what:-should-fail-on-this-internal-error}
		if (( $? > 0 ))
		then
			echo "ERR: unable to add one or more of: $what  [FAIL]"
			cleanup 1
		fi
	fi

	if [[ $cmd != "add" ]]				# already done, run anything else that would continue to process the files
	then
		if [[ -n $author ]]
		then
			author="'$author'"
		fi
		verbose "stashing modifications to ${what:-everything} with $cmd"
		if (( smsg_ok < 1 )) && [[ -n $usrmsg ]]			# short messages not allowed; forces git to prompt for user message
		then
			$forreal git $cmd  $git_opts  ${what}
		else
			if [[ -z $usrmsg ]]
			then
				echo git $cmd $author $git_opts  ${what}
				$forreal git $cmd  $author $git_opts ${what}

			else
				echo git $cmd $author $git_opts $um_opt "$usrmsg"  ${what}
				$forreal git $cmd  $author $git_opts $um_opt "$usrmsg"  ${what}
			fi
		fi
	fi
fi

if (( push > 0 ))
then
	if (( gerrit_push ))
	then
		errs=0
		rev_str=""
		for r in $reviewer
		do
			if grep "^${r}[ 	]" $HOME/lib/reviewers | head -1 | read junk mail
			then
				echo "adding reviewer: $junk == $mail"
				rev_str+="%r=$mail"
			else
				echo "unable to find reviewer: $r"
				errs=1
			fi
		done
		if (( errs ))
		then
			exit 1
		fi

		echo "INFO: pusing to gerrit for review: branch == $gerrit_branch"
		$trace_on
																						# for gerrit we always must push to master, no concept of dev branch
		$forreal git push $remote HEAD:refs/for/${gerrit_branch}${wip}${rev_str}		# wip set with either -W or -R options for wip or ready
		set -x
	else
		echo "INFO: pushing current $branch to $remote"
		$forreal git push $remote $branch
	fi
fi


cleanup $?

